import React,{useState, useEffect} from 'react';
import{ useParams} from "react-router-dom";

// Aqui creamos las tarjetas detalladas cuando seleccionamos un personaje
// Aprovechando el codigo creado en Cards, en este caso fraccionamos la informacion de la Api fetched para obtener cada dato por separado
const CardDetails = () => {
    let {id} = useParams();
    let [fetchedData, updateFetchData] = useState([]);
    let {name, image, location, origin, gender, species, status, type} = fetchedData;
    let api = `https://rickandmortyapi.com/api/character/${id}`;
    useEffect(() => {
        (async function () {
          let data = await fetch(api).then((res) => res.json());
          updateFetchData(data);
        })();
      }, [api]);

    
  return (
    <div className="container d-flex justify-content-center">
      <div className="d-flex flex-column gap-3">
        <h1 className="text-center">{name}</h1>
        <img src={image} alt="" className="img-fluid" />
        {(()=>{
          if(status === "Dead") {
            return(
              <div className="badge bg-danger">{status}</div>
            )
          }
          else if(status === "Alive") {
            return(
              <div className="badge bg-success">{status}</div>
            )
          }
          else{
            return(
              <div className="badge bg-secondary">{status}</div>
            )
          }
        })()}
        <div className="content">
            <div className="">
                <spam className="fw-bold">
                    Gender : 
                </spam>
                {gender}
            </div>

            <div className="">
                <spam className="fw-bold">
                    Species : 
                </spam>
                {species}
            </div>

            <div className="">
                <spam className="fw-bold">
                    Location : 
                </spam>
                {location?.name}
            </div>

            <div className="">
                <spam className="fw-bold">
                    Origin : 
                </spam>
                {origin?.name}
            </div>

            <div className="">
                <spam className="fw-bold">
                    Characteristic : 
                </spam>
                {type ===""? "None" : type}
            </div>

        </div>
      </div>
    </div>
  )
};

export default CardDetails;
