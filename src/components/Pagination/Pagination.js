import React, {useState, useEffect} from 'react';
import ReactPaginate from "react-paginate";

// Aqui creo la paginacion de la aplicacion utilizando react-paginate

const Pagination = ({info, pageNumber, setPageNumber}) => {

    let [width, setWidth] = useState(window.innerWidth);

    // Aqui utilizo useEffect para capturar las dimensiones de la pantalla segun se reescala para poder 
    // hacer responsive nuestra paginacion (Haciendo que si la pantalla es mas pequeña los botones de prev y next desaparezcan)
    let updateDimension = ()=>{
        setWidth(window.innerWidth)
    }
    
    useEffect(() => {
        window.addEventListener("resize", updateDimension);
        return () => window.removeEventListener("resize", updateDimension);
    }, []);
return (
<>
<style jsx>
    {`
    @media (max-width:768px) {
        .next, .prev {
            display : none;
        }
    }
    `} 

</style>
<ReactPaginate
className= " pagination justify-content-center gap-4 my-4"
forcePage={pageNumber === 1 ? 0 : pageNumber -1}
nextLabel="Next"
previousLabel="Prev"
nextClassName="btn btn-light next"
previousClassName="btn btn-light prev"
pageClassName="page-item"
pageLinkcClassName="page-link"
activeClassName="active"
onPageChange={(data) => {setPageNumber(data.selected +1);}}
pageCount={info?.pages}   
/>
</>
);
};

export default Pagination;
